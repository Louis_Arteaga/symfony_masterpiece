<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FestivalRepository")
 */
class Festival
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $place;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="festival")
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Artist", inversedBy="festivals")
     */
    private $artist;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Stand", mappedBy="festival")
     */
    private $stand;

    public function __construct()
    {
        $this->artist = new ArrayCollection();
        $this->stand = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getPlace(): ?string
    {
        return $this->place;
    }

    public function setPlace(string $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Artist[]
     */
    public function getArtist(): Collection
    {
        return $this->artist;
    }

    public function addArtist(Artist $artist): self
    {
        if (!$this->artist->contains($artist)) {
            $this->artist[] = $artist;
        }

        return $this;
    }

    public function removeArtist(Artist $artist): self
    {
        if ($this->artist->contains($artist)) {
            $this->artist->removeElement($artist);
        }

        return $this;
    }

    /**
     * @return Collection|Stand[]
     */
    public function getStand(): Collection
    {
        return $this->stand;
    }

    public function addStand(Stand $stand): self
    {
        if (!$this->stand->contains($stand)) {
            $this->stand[] = $stand;
            $stand->setFestival($this);
        }

        return $this;
    }

    public function removeStand(Stand $stand): self
    {
        if ($this->stand->contains($stand)) {
            $this->stand->removeElement($stand);
            // set the owning side to null (unless already changed)
            if ($stand->getFestival() === $this) {
                $stand->setFestival(null);
            }
        }

        return $this;
    }
}

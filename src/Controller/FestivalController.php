<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use JMS\Serializer\SerializerInterface;
use App\Repository\FestivalRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Festival;
use App\Form\FestivalType;

/**
 * @Route("/api/festival")
 */

class FestivalController extends AbstractController{

    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(SerializerInterface $serializer) {
        $this->serializer = $serializer;
    }

    /**
     * @Route(methods="GET")
     */
    public function allFestivals(FestivalRepository $repo){
        $festivals = $repo->findAll();
        $json = $this->serializer->serialize($festivals, 'json');

        return new JsonResponse($json, JsonResponse::HTTP_OK, [], true);
    }

    /**
     * @Route(methods="POST")
     */
    
    public function addFestival(ObjectManager $manager, Request $request){
        $festival = new Festival();
        $form = $this->createForm(FestivalType::class, $festival);

        $form->submit(json_decode($request->getContent(), true));
        
        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($festival);
            $manager->flush();

            return new JsonResponse($this->serializer->serialize($festival, 'json'), JsonResponse::HTTP_CREATED,[], true);
        }
        return $this->json($form->getErrors(true), JsonResponse::HTTP_BAD_REQUEST);
    }
    /**
     * @Route("/{festival}", methods="DELETE")
     */
    public function removeFestival(Festival $festival, ObjectManager $manager){
        $manager->remove($festival);
        $manager->flush();

        return $this->json('', JsonResponse::HTTP_NO_CONTENT);
    }
    /**
     * @Route("/{festival}", methods="PATCH")
     */
    public function modifyFestival(Festival $festival,ObjectManager $manager, Request $request){
        $form = $this->createForm(FestivalType::class, $festival);

        $form->submit(json_decode($request->getContent(), true), false);

        if($form->isSubmitted() && $form->isValid()){
            $manager->flush();

            return new JsonResponse($this->serializer->serialize($festival, 'json'), JsonResponse::HTTP_OK,[], true);
        }
    }
    /**
     * @Route("/{festival}", methods="GET")
     */
    public function oneFestival(Festival $festival){
        return new JsonResponse($this->serializer->serialize($festival, 'json'), JsonResponse::HTTP_OK, [], true);
    }

}
    
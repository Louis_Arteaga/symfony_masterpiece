<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\Serializer\SerializerInterface;
use App\Form\UserType;

/**
 * @Route("/api/user", name="api_user")
 */

class UserController extends AbstractController
{

    /**
     * @var SerializerInterface
     */
    private $serializer;
    public function __construct(SerializerInterface $serializer) {
        $this->serializer = $serializer;
    }

    /**
     * @Route(methods="POST")
     */
    public function addUser(Request $request,
     UserPasswordEncoderInterface $encoder, ObjectManager $manager){
        
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->submit(json_decode($request->getContent(), true));

        if($form->isSubmitted() && $form->isValid()) {
            
            $user->setPassword($encoder->encodePassword($user, $user->getPassword()));
            $manager->persist($user);
            $manager->flush();
            
            return new JsonResponse($this->serializer->serialize($user, 'json'), JsonResponse::HTTP_CREATED, [], true);

        }

        return $this->json($form->getErrors(true), JsonResponse::HTTP_BAD_REQUEST);

    }

}

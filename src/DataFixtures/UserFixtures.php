<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture{
    
    private $encoder;
    public const USER_REFERENCE = 'user';

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail('test@test.com');
        $user->setName('Len Faki');
        $user->setPassword($this->encoder->encodePassword($user, '1234'));
        $manager->persist($user);

        $user = new User();
        $user->setEmail('test1@test.com');
        $user->setName('Amelie Lens');
        $user->setPassword($this->encoder->encodePassword($user, '1234'));
        $manager->persist($user);

        $user = new User();
        $user->setEmail('test2@test.com');
        $user->setName('Solomun');
        $user->setPassword($this->encoder->encodePassword($user, '1234'));
        $manager->persist($user);

        $user = new User();
        $user->setEmail('test3@test.com');
        $user->setName('Paula Temple');
        $user->setPassword($this->encoder->encodePassword($user, '1234'));
        $manager->persist($user);

        $user = new User();
        $user->setEmail('test4@test.com');
        $user->setName('AnD');
        $user->setPassword($this->encoder->encodePassword($user, '1234'));
        $manager->persist($user);

        $user = new User();
        $user->setEmail('test5@test.com');
        $user->setName('Marcel Dettmann');
        $user->setPassword($this->encoder->encodePassword($user, '1234'));
        $manager->persist($user);

        $manager->flush();
    }
}

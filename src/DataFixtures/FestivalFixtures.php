<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Festival;
use Symfony\Component\Validator\Constraints\DateTime;

class FestivalFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
       $festival = new Festival();
       $festival->setName('Nuits sonores');
       $festival->setDate(new \DateTime('29-05-2019'));
       $festival->setPlace('Lyon');
       $manager->persist($festival);

       $festival = new Festival();
       $festival->setName('Insane');
       $festival->setDate(new \DateTime('14-07-2019'));
       $festival->setPlace('Avignon');
       $manager->persist($festival);

       $festival = new Festival();
       $festival->setName('DGTL');
       $festival->setDate(new \DateTime('23-08-2019'));
       $festival->setPlace('Barcelone');
       $manager->persist($festival);

       $festival = new Festival();
       $festival->setName('Sonar');
       $festival->setDate(new \DateTime('18-07-2019'));
       $festival->setPlace('Barcelone');
       $manager->persist($festival);
        
       $manager->flush();
    }
}
